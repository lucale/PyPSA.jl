module PyPSA

using CondaPkg
CondaPkg.add("pypsa")

using PythonCall
pypsa = pybuiltins.None
function __init__()
    pypsa = pyimport("pypsa")
end
export pypsa

end 
