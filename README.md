# PyPSA.jl

Julia wrapper around the [PyPSA](https://github.com/pypsa/pypsa) Python library using [PythonCall](https://github.com/JuliaPy/PythonCall.jl).  

## Installation 
Install Julia package using the gitlab URL.

```julia
using Pkg; Pkg.add("https://gitlab.pik-potsdam.de/lucale/PyPSA.jl")
```

It will create a conda environment and install the `pypsa` library.


## How to use 
Once the installation is complete you can use the library with 
```julia
using PyPSA: pypsa

n = pypsa.Network()  
# ... 
```
Take a look at the [documentation of PyPSA](https://pypsa.readthedocs.io/en/latest) for more information.

## License
Open source GPLv3 licence.

